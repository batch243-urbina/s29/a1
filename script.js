// FINDING USING OR OPERATOR

db.users.find(
  {
    $or: [{ firstName: "s" }, { lastName: "d" }],
  },
  {
    firstName: 1,
    lastName: 1,
    _id: 0,
  }
);

// FINDING USERS FROM HR DEPARTMENT

db.users.find({ $and: [{ department: "HR" }, { age: { $gte: 70 } }] });

// FINDING E AND AGE LESS THAN 30

db.users.find({
  $and: [{ firstName: { $regex: "e" } }, { age: { $lte: 30 } }],
});
